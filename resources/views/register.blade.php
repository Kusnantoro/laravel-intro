<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form registrasi Sanberbook</title>
</head>

<body>
    <h1>Create New Account!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
    @csrf
        <!-- text input -->
        <label>First Name:</label><br><br>
        <input type="text" name="firstName" placeholder="First Name.."><br><br>
        <label>Last Name:</label><br><br>
        <input type="text" name="lastName" placeholder="Last Name.."><br><br>
        <!-- Radio-buttons -->
        <label>Gender:</label><br><br>
        <input type="radio" name="gender" value="male">
        <label>Male</label><br>
        <input type="radio" name="gender" value="female">
        <label>Female</label><br>
        <input type="radio" name="gender" value="other">
        <label>Other</label><br><br>
        <!-- Selected -->
        <label>Nationality:</label><br><br>
        <select name="Country">
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Singapura">Singapura</option>
        </select><br><br>
        <!-- Checkbox -->
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="bahasa1" value="bIndonesia">
        <label for="bahasa1">Bahasa Indonesia</label><br>
        <input type="checkbox" name="bahasa2" value="english">
        <label for="bahasa2">English</label><br>
        <input type="checkbox" name="bahasa3" value="other">
        <label for="bahasa3">Other</label><br><br>
        <!-- TextArea -->
        <label>Bio:</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        <!-- Submit -->
        <input type="submit" value="Sign Up">
    </form>
</body>

</html>