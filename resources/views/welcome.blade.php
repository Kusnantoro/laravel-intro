<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Selamat Datang di Sanberbook</title>
</head>

<body>
    <div style="text-align: center;">
        <h1>Selamat Datang {{$firstName}} {{$lastName}}</h1>
        <h3>Terima kasih telah bergabung di Sanberbook. Sosial Media kita bersama!</h3>
    </div>
</body>

</html>