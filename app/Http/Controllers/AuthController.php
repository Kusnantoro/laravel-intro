<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form() {
        return view('register');
    }

    public function halo(Request $request) {
        $firstName = $request['firstName'];
        $lastName = $request['lastName'];
        return view('welcome', compact('firstName', 'lastName'));
    }
}